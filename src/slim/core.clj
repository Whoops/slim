(ns slim.core)

(defprotocol Renderable
  (render [self]))

(defn- render-with-attrs [tag attrs children]
  (let [child-nodes (mapv render children)
        tag (name tag)]
    `(create-element ~tag ~attrs ~child-nodes)))
(defn- render-plain [tag children]
  (render-with-attrs tag {} children))

(defn render-vec [[tag & args]]
  (if (map? (first args))
    (render-with-attrs tag (first args) (rest args))
    (render-plain tag args)))

(defn render-call [self]
  (if (= (first self) 'clojure.core/deref)
    `(text-node (deref ~(last self)) ~(last self))
    `(.warn js/console "Looks like you called a function. That might hide dependencies from me.")))

(extend-protocol Renderable
  String
  (render [self] `(text-node ~self))
  clojure.lang.IPersistentVector
  (render [self] (render-vec self))
  clojure.lang.IPersistentList
  (render [self] (render-call self)))

#_(def name (atom "Walton"))

(render [:div {:class "foo"}
         [:h1 "Clojure Slims"]
         [:h2 {:class "subtitle"} "foo"]])

(defmacro defcomponent
  "Creates a component. Document me better"
  [name body]
  `(defn ~name [~'target]
     (let [~'ele ~(render body)]
       (.appendChild ~'target ~'ele))))


(clojure.pprint/pprint (macroexpand-1 '(defcomponent main [:div {:class "foo"}
                                                           [:h1 "Clojure Slims!"]
                                                           [:h2 {:class "subtitle"} "A proof of concept"]
                                                           [:div
                                                            [:input {:id    "name"
                                                                     :input input-name}]
                                                            [:p "Hello " @me]]])))
