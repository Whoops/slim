(ns slim.core)

(def events [:input :click])

(defn text-node
  ([s a]
   (let [ele (.createTextNode js/document s)]
     (add-watch a :update
                (fn [_ _ _ new-state]
                  (aset ele "nodeValue" new-state)))
     ele))
  ([s]
   (.createTextNode js/document s)))

(defn create-element [tag attrs children]
  (let [handlers (select-keys attrs events)
        attrs (apply dissoc attrs events)
        ele (.createElement js/document tag)]
    (doseq [[attr val] attrs]
      (.setAttribute ele (name attr) val))
    (doseq [[event handler] handlers]
      (.addEventListener ele (name event) handler))
    (doseq [child children]
      (.appendChild ele child))
    ele))
