(ns ^:figwheel-hooks demo.core
  (:require
   [slim.core :refer-macros [defcomponent]]))

#_(macroexpand-1 '(defcomponent home
                    [:div {} "Hello World!!"]))

#_(cljs.pprint/pprint
 (macroexpand-1 '(defcomponent main [:div {:class "foo"}
                                [:h1 "Clojure Slims"]
                                     [:h2 {:class "subtitle"} "A proof of concept"]])))

(defn event-value [event]
  (-> event .-target .-value))

(def me (atom ""))

(defn input-name [event]
  (reset! me (event-value event)))

(defcomponent main [:div {:class "foo"}
                    [:h1 "Clojure Slims!"]
                    [:h2 {:class "subtitle"} "A proof of concept"]
                    [:div
                     [:p "Hello " @me]
                     [:input {:id "name"
                              :input input-name}]]])


(let [app (.getElementById js/document "app")]
  (aset app "innerHTML" "")
  (main app))

;; specify reload hook with ^;after-load metadata
(defn ^:after-load on-reload []
  ;; optionally touch your app-state to force rerendering depending on
  ;; your application
  ;; (swap! app-state update-in [:__figwheel_counter] inc)
  )
